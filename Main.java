public class Main
{
    public static void main(String[] args)
    {
        String txt = "Object-oriented programming is a programming language model organized around objects rather than actions and data rather than logic. " +
                "Object-oriented programming... " +
                "Object-oriented programming is a programming paradigm that relies on the concept of classes and objects. " +
                "Object-oriented programming... " +
                "Object-oriented programming is a fundamental programming paradigm used by nearly every developer at some point in their career. " +
                "Object-oriented programming is the most popular programming paradigm. ";

        String source_str = "object-oriented programming";
        String replacement_str = "OOP";
        String tmp = "";

        tmp = txt.toLowerCase();

        if (tmp.contains(source_str))
        {
            int replacement_count = (tmp.length() - tmp.replace(source_str, "").length()) / source_str.length();

            for (int i = replacement_count; i > 0; i--)
            {
                if (i % 2 == 0)
                {
                    txt = txt.substring(0, tmp.lastIndexOf(source_str)) + replacement_str + txt.substring(tmp.lastIndexOf(source_str) + source_str.length());
                }
                tmp = tmp.substring(0, tmp.lastIndexOf(source_str)) + source_str.toUpperCase() + tmp.substring(tmp.lastIndexOf(source_str) + source_str.length());
            }

        }

        System.out.println(txt);
    }

}