public class Main_3
{
    public static boolean is_only_latin_symbols(String word)
    {
        for (int i = 0; i < word.length(); i++)
        {
            if (!(word.charAt(i) >= 65 && word.charAt(i) <= 90 || word.charAt(i) >= 97 && word.charAt(i) <= 122) )
            {
                return false;
            }
        }

        return true;
    }

    public static void main(String[] args)
    {
        String txt = "Hi man, как дела? Valorant сегодня играем? Или you slave?";
        String[] words = txt.replaceAll("[^A-Za-zА-Яа-я-\s]", "").split(" ");
        int latin_words_count = 0;
        for (int i = 0; i < words.length; i++)
        {
            if (is_only_latin_symbols(words[i]))
            {
                latin_words_count++;
            }
        }

        System.out.println(latin_words_count);
    }
}
