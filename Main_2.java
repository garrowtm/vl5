import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main_2
{
    public static int get_letters_count(String word)
    {
        int word_length = word.length();
        Pattern pattern = Pattern.compile("(.)(?=.*(\\1))");
        Matcher matcher = pattern.matcher(word);
        while (matcher.find())
        {
            word_length--;
        }

        return word_length;
    }

    public static void main(String[] args)
    {
        String txt = "fffff ab f 1234 jkjk";
        String[] words = txt.split(" ");
        int min_index = 0;
        int min_len = get_letters_count(words[0]);
        for (int i = 0; i < words.length; i++)
        {
            if (get_letters_count(words[i]) < min_len)
            {
                min_index = i;
                min_len = get_letters_count(words[i]);
            }
            if (min_len == 1)
            {
                break;
            }
        }

        System.out.println(words[min_index] + " " + min_len);
    }
}
