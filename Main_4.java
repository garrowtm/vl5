import java.util.*;

public class Main_4
{
    public static boolean is_palindrome(String word)
    {
        word = word.toLowerCase();
        for (int i = 0; i < word.length(); i++)
        {
            if (word.charAt(i) != word.charAt(word.length() - i - 1))
            {
                return false;
            }
        }

        return true;
    }


    public static void main(String[] args)
    {
        String txt = "Tenet сегодня смотрим (это который Довод)?\"";
        String[] words = txt.replaceAll("[^A-Za-zА-Яа-я-\s]", "").split(" ");
        ArrayList<String>  palindrome_words = new ArrayList<>();
        for (int i = 0; i < words.length; i++)
        {
            if (is_palindrome(words[i]))
            {
                palindrome_words.add(words[i]);
            }
        }

        for (int i = 0; i < palindrome_words.size(); i++)
        {
            System.out.println(palindrome_words.get(i));
        }
    }
}
